# README #

A simple weather app with integrated google maps.

### What is this repository for? ###

* This is a Vue assessment project.

### How do I get set up? ###

Create an .env file in the project root folder, and add these 2 items :-

* VUE_APP_GOOGLE_API=[YOUR_API_KEY]
* VUE_APP_WEATHER_API=[YOUR_API_KEY]

Both Maps & Weather API will not run without the proper keys.

Vue app must be restarted (not reloaded) for any .env changes to take place.

### Who do I talk to? ###

* amirzaki.zainal@gmail.com
* amirzaki@atherons.com