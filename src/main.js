import Vue from 'vue'
import App from './App.vue'
require('dotenv').config()

import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: process.env.VUE_APP_GOOGLE_API,
    libraries: 'places'
  },

})

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')